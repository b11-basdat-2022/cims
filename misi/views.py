from django.shortcuts import render, redirect
from django.http.response import HttpResponse
from django.core import serializers
from django.contrib import messages

# Create your views here.
from django.db import connection
from collections import namedtuple

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]


def misi_utama(request):
    if 'username' not in request.session:
        return redirect('/login')

    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                            SELECT MU.nama_misi, count(nama_tokoh) AS jumlah_dijalankan
                            FROM CIMS.MISI_UTAMA MU
                            LEFT OUTER JOIN CIMS.MENJALANKAN_MISI_UTAMA MMU
                            ON MU.nama_misi = MMU.nama_misi
                            GROUP BY MU.nama_misi
                            ;
                            """)
            result = namedtuplefetchall(cursor)
        except Exception as e:
            print(e)
        finally:
            cursor.close()

        return render(request, 'list_misi_utama.html', {'result': result, 'role': request.session['role']})

def detail_misi_utama(request, nama):
    if 'username' not in request.session:
        return redirect('/login')

    with connection.cursor() as cursor:
        try:
            cursor.execute("SELECT * FROM CIMS.MISI WHERE NAMA = '" + str(nama) + "';")
            result = namedtuplefetchall(cursor)
        except Exception as e:
            print(e)
        finally:
            cursor.close()

        return render(request, 'detail-misi-utama.html', {'result': result, 'role': request.session['role']})


def read_menjalankan_misi(request):
    if 'username' not in request.session:
        return redirect('/login')

    with connection.cursor() as cursor:
        result = None
        try:
            if request.session['role'] == 'pemain':
                query = f"""SELECT * FROM CIMS.MENJALANKAN_MISI_UTAMA WHERE username_pengguna = '{request.session['username']}';"""
            else:
                query = "SELECT * FROM CIMS.MENJALANKAN_MISI_UTAMA;"
            cursor.execute(query)
            result = namedtuplefetchall(cursor)
        except Exception as e:
            print(e)
        finally:
            cursor.close()

        if result != None:
            return render(request, 'list_menjalankan_misi.html', {'result': result, 'role': request.session['role']})
        else:
            return render(request, 'list_menjalankan_misi.html', {'role': request.session['role']})


def create_menjalankan_misi(request):
    if 'username' not in request.session:
        return redirect('/login')

    with connection.cursor() as cursor:
        result = None
        tokoh = None
        misi = None
        try:
            if request.method == 'POST':
                #I need to complete this part :v
                nama_tokoh = request.POST['tokoh']
                nama_makanan = request.POST['misi']
                if nama_tokoh == " " or nama_makanan == " ":
                    messages.info(request, 'Data insufficient')
                else:
                    #insert and stuff should be here
                    return redirect(misi_utama)

            if request.session['role'] == 'admin':
                return redirect(misi_utama)

            query = f"""SELECT nama FROM CIMS.TOKOH WHERE username_pengguna = '{request.session['username']}';"""
            cursor.execute(query)
            tokoh = namedtuplefetchall(cursor)

            query = f"""SELECT nama_misi FROM CIMS.MISI_UTAMA;"""
            cursor.execute(query)
            misi = namedtuplefetchall(cursor)
        except Exception as e:
            print(e)
        finally:
            cursor.close()

        if result != None:
            return render(request, 'create_menjalankan_misi.html', {'result': result, 'tokoh': tokoh, 'misi': misi, 'role': request.session['role']})
        else:
            return render(request, 'create_menjalankan_misi.html', {'tokoh': tokoh, 'misi': misi, 'role': request.session['role']})



def check_data(data):
    if (data is None) or data == '':
        return False
    else:
        return True

def delete_misi_utama(request, nama):
    if 'username' not in request.session:
        return redirect('/login')

    with connection.cursor() as cursor:
        result = None
        try:
            query = f"""
                     DELETE FROM CIMS.MISI
                     WHERE nama = '{nama}';
                     """
            cursor.execute(query)

        except Exception as e:
            print(e)
        finally:
            cursor.close()

        return redirect(misi_utama)


def create_misi_utama(request):
    if 'username' not in request.session:
        return redirect('/login')

    with connection.cursor() as cursor:
        result = None
        try:
            if request.method == 'POST':
                #I need to complete this part :v
                nama = request.POST['nama']
                efek_energi = request.POST['efek_energi']
                efek_hubungan = request.POST['efek_hubungan']
                efek_kelaparan = request.POST['efek_kelaparan']
                syarat_energi = request.POST['syarat_energi']
                syarat_hubungan = request.POST['syarat_hubungan']
                syarat_kelaparan = request.POST['syarat_kelaparan']
                completion_time = request.POST['completion_time']
                reward_koin = request.POST['reward_koin']
                reward_xp = request.POST['reward_xp']
                deskripsi = request.POST['deskripsi']
                if (check_data(nama) and
                    check_data(efek_energi) and
                    check_data(efek_hubungan) and
                    check_data(efek_kelaparan) and
                    check_data(syarat_energi) and
                    check_data(syarat_hubungan) and
                    check_data(syarat_kelaparan) and
                    check_data(completion_time) and
                    check_data(reward_koin) and
                    check_data(reward_xp) and
                    check_data(deskripsi)) == False:
                    messages.info(request, 'Data insufficient')
                else:
                    #insert and stuff should be here
                    #insert to misi
                    #insert to misi_utama
                    query = f"""INSERT INTO CIMS.MISI (nama,
                                efek_energi,
                                efek_hubungan_sosial,
                                efek_kelaparan,
                                syarat_energi,
                                syarat_hubungan_sosial,
                                syarat_kelaparan,
                                completion_time,
                                reward_koin,
                                reward_xp,
                                deskripsi)
                                VALUES ('{nama}',
                                {efek_energi},
                                {efek_hubungan},
                                {efek_kelaparan},
                                {syarat_energi},
                                {syarat_hubungan},
                                {syarat_kelaparan},
                                '{completion_time}',
                                {reward_koin},
                                {reward_xp},
                                '{deskripsi}'
                                );
                            """
                    cursor.execute(query)

                    query = f"""INSERT INTO CIMS.MISI_UTAMA (nama_misi)
                            VALUES ('{nama}');
                            """
                    cursor.execute(query)
                    return redirect(misi_utama)

            if request.session['role'] == 'pemain':
                return redirect(misi_utama)

        except Exception as e:
            print(e)
        finally:
            cursor.close()

        return render(request, 'create_misi_utama.html', {'result': result, 'role': request.session['role']})


def update_menjalankan_misi(request, nama_tokoh, nama_misi, waktu):
    if 'username' not in request.session:
        return redirect('/login')

    with connection.cursor() as cursor:
        result = None
        try:
            if request.method == 'POST':
                #I need to complete this part :v
                status = request.POST['status']
                if check_data(status) == False:
                    messages.info(request, 'Data insufficient')
                else:
                    #insert and stuff should be here
                    #insert to misi
                    #insert to misi_utama
                    return redirect(read_menjalankan_misi)

            if request.session['role'] == 'admin':
                return redirect(read_menjalankan_misi)

            query = f""";
                     """

        except Exception as e:
            print(e)
        finally:
            cursor.close()

        return render(request, 'create_misi_utama.html', {'result': result, 'role': request.session['role']})
