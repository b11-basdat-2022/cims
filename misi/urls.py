from django.urls import path
from .views import *

urlpatterns = [
    path('daftar-misi-utama', misi_utama, name='misi-utama'),
    path('detail-misi-utama/<str:nama>/)', detail_misi_utama, name='detail-misi-utama'),
    path('daftar-menjalankan-misi', read_menjalankan_misi, name='read-menjalankan-misi'),
    path('create-menjalankan-misi', create_menjalankan_misi, name='create-menjalankan-misi'),
    path('create-misi-utama', create_misi_utama, name='create-misi-utama'),
    path('delete-misi-utama/<str:nama>/', delete_misi_utama, name='delete-misi-utama')
]
