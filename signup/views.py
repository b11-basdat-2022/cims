from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.db import connection
from .forms import SignupAdminForm, SignupPemainForm

# Redirect in case somebody just type "signup/"
def signup_redirect(request):
    return redirect('/signup/pemain')

# And this is when the user successfully creates their account
def signup_success(request):
    return render(request, 'signup_success.html')

# Signup for Admin
def signup_admin(request):
    if 'username' in request.session:
        return redirect('/')

    response = {}
    form = SignupAdminForm(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        # There will be no password checking 
        # because the app description doesn't even care about it.
        response['username'] = request.POST['username']
        response['password'] = request.POST['password']
        response['error_msg'] = check_user_exists(
                response['username'], None)

        if response['error_msg'] == "":
            create_admin(response['username'], response['password'])
            return redirect('/signup/success')

    response['form'] = form

    return render(request, 'signup_admin.html', context = response)

# Signup for Pemain 
def signup_pemain(request):
    if 'username' in request.session:
        return redirect('/')

    response = {}
    form = SignupPemainForm(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        response['username'] = request.POST['username']
        response['email'] = request.POST['email']
        response['nohp'] = request.POST['nohp']
        response['password'] = request.POST['password']
        response['error_msg'] = check_good_nohp(
                response['username'], response['email'], response['nohp'])

        if response['error_msg'] == "":
            create_pemain(response['username'], response['email'],
                    response['nohp'], response['password'])
            return redirect('/signup/success')

    response['form'] = form

    return render(request, 'signup_pemain.html', context = response)

# Check for existing username
# We're using chain of responsibility pattern
# check_good_nohp -> check_user_exists -> check_email_exists
def check_good_nohp(un, em, no):
    if no.isnumeric():
        return check_user_exists(un, em)
    else:
        return "Nomor HP tidak benar."

def check_user_exists(un, em):
    with connection.cursor() as cursor:
        query = f"""select username from cims.akun
        where username = '{un}';"""
        cursor.execute(query)
        out = cursor.fetchall()

        if (len(out) == 0):
            if em == None:
                return ""
            else:
                return check_email_exists(em)
        else:
            return f"""Username '{un}' sudah terdaftar."""

# Check for existing email
def check_email_exists(em):
    with connection.cursor() as cursor:
        query = f"""select email from cims.pemain
        where email = '{em}';"""
        cursor.execute(query)
        out = cursor.fetchall()

        if (len(out) == 0):
            return ""
        else:
            return f"""Email '{em}' sudah terdaftar."""

# Create our account finally 
def create_admin(un, pw):
    with connection.cursor() as cursor:
        query = f"""insert into cims.akun values
        ('{un}');
        insert into cims.admin values
        ('{un}','{pw}');"""
        cursor.execute(query)

def create_pemain(un, em, no, pw):
    with connection.cursor() as cursor:
        query = f"""insert into cims.akun values
        ('{un}');
        insert into cims.pemain values
        ('{un}','{em}','{pw}','{no}',0);"""
        cursor.execute(query)
