from django import forms

class SignupAdminForm(forms.Form):
    username = forms.CharField(
        label = 'Username',
        max_length = 50,
        required = True
    )
    password = forms.CharField(
        label = 'Password',
        max_length = 20,
        required = True,
        widget = forms.PasswordInput()
    )


class SignupPemainForm(forms.Form):
    username = forms.CharField(
        label = 'Username',
        max_length = 50,
        required = True
    )
    email = forms.CharField(
        label = 'Email',
        max_length = 50,
        required = True,
    )
    nohp = forms.CharField(
        label = 'No HP',
        max_length = 12,
        required = True
    )
    password = forms.CharField(
        label = 'Password',
        max_length = 20,
        required = True,
        widget = forms.PasswordInput()
    )

