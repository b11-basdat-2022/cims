from django.urls import path
from .views import signup_admin, signup_pemain, signup_success, signup_redirect

urlpatterns = [
    path('', signup_redirect),
    path('admin', signup_admin, name='signup-admin'),
    path('pemain', signup_pemain, name='signup-pemain'),
    path('success', signup_success, name='signup-success')
]
