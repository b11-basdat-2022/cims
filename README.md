![simps](cims.png)
---
Link: https://cims-b11.herokuapp.com

For every developers and teaching assistants, please take a look at the following guidelines before doing anything in this repo:  
[[GUIDELINES LINK]](GUIDELINES.md)

All info about views and stored procedures can be found here:  
[[SQL INFO LINK]](SQL-INFO.md)

---
Do you hate reality? Are you tired of hearing news about nuclear annihilation, China taking over the world, or the fact you are hiding a deep secret about the death of your older sister? All of that suck but we know we are all want to escape from reality and have inner peace for ourselves. 

The Cims is a "game" that is "inspired" by the likeness of EA's The Sims without the company ruining it with million of DLCs nobody asks for. The Cims lets you create one or multiple characters to live a perfect city life without unnecessary stressful days. You can create a perfect version of yourself, your alter ego, your friend, your crush, your favorite fictional character, or even your own unique characters as a playable character with hundreds of customization option available. 

Show your inner narcissism by making everyone's lives as perfect as you want. Make them chat with other characters (your own or someone else's), do fun jobs, complete missions, earn money and EXP, and tend their own daily needs such as hunger, energy, and interaction. Be careful that they can die from starvation, exhaustion, and being alone for too long and if you are out of playable characters, it's game over, man! Not much different from our reality but where's the fun of not having lose conditions? This is the **better** reality afterall.


This is the reality you have near full control of. Isn't this what you want? 

# Features
Coming Soon!

# Developers (Group B11)
We work in one group under the name of "B11" from class B of database subject. We hate reality as much as you do.

| NAME                  | ID (NPM)   |
| ----------------------| ---------- |
| Regina Febrian Pehry  | 2006596882 |
| Anisa Maharani        | 2006483100 |
| Jeremy Christianto    | 2006462885 |
| Fawzan Fawzi Yusuf    | 2006595910 |

# License
AGPLv3 License.  
Yes, we will open source our software to show the world how bad we are at writing codes.




