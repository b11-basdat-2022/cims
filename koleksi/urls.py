from django.urls import path
from .views import menu_list_koleksi, list_koleksi_rambut, list_koleksi_mata, list_koleksi_rumah, list_koleksi_barang, list_koleksi_apparel, menu_buat_koleksi

# buat_koleksi_rambut, buat_koleksi_mata, buat_koleksi_rumah, buat_koleksi_barang, buat_koleksi_apparel

urlpatterns = [
    path('daftar-koleksi', menu_list_koleksi, name='daftar-koleksi'),
    path('detail-list-koleksi-rambut', list_koleksi_rambut, name='list-koleksi-rambut'),
    path('detail-list-koleksi-mata', list_koleksi_mata, name='list-koleksi-mata'),
    path('detail-list-koleksi-rumah', list_koleksi_rumah, name='list-koleksi-rumah'),
    path('detail-list-koleksi-barang', list_koleksi_barang, name='list-koleksi-barang'),
    path('detail-list-koleksi-apparel', list_koleksi_apparel, name='list-koleksi-apparel'),
    path('buat-koleksi', menu_buat_koleksi, name='buat-koleksi'),
    # path('buat-koleksi-rambut', buat_koleksi_rambut, name='buat-koleksi-rambut'),
    # path('buat-koleksi-mata', buat_koleksi_mata, name='buat-koleksi-mata'),
    # path('buat-koleksi-rumah', buat_koleksi_rumah, name='buat-koleksi-rumah'),
    # path('buat-koleksi-barang', buat_koleksi_barang, name='buat-koleksi-barang'),
    # path('buat-koleksi-apparel', buat_koleksi_apparel, name='buat-koleksi-apparel')
]
