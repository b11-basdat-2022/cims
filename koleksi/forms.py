from django import forms

class CreateKoleksiRambut(forms.Form):
    harga_jual = forms.IntegerField(
        label = 'Harga Jual',
        type = 'integer',
        required = True
    )

    tipe_rambut = forms.CharField(
        label = 'Tipe Rambut',
        max_length = 10,
        required = True
    )