from django.shortcuts import redirect, render
from django.http.response import HttpResponse
from django.core import serializers
from django.db import connection
from collections import namedtuple

# Create your views here.

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def menu_list_koleksi(request):
    if 'username' not in request.session:
        return redirect('/login')

    return render(request, 'menu_list_koleksi.html', {'role': request.session['role']})

def list_koleksi_rambut(request):
    if 'username' not in request.session:
        return redirect('/login')

    with connection.cursor() as cursor:
        try:
            query = f"""SELECT R.id_koleksi, K.harga, R.tipe 
                    FROM CIMS.KOLEKSI K, CIMS.RAMBUT R 
                    WHERE K.id=R.id_koleksi;"""
            cursor.execute(query)
            result = namedtuplefetchall(cursor)
        except Exception as e:
            print(e)
        finally:
            cursor.close()
        
        return render(request, 'list_koleksi_rambut.html', {'result': result, 'role': request.session['role']})

def list_koleksi_mata(request):
    if 'username' not in request.session:
        return redirect('/login')

    with connection.cursor() as cursor:
        try:
            query = f"""SELECT M.id_koleksi, K.harga, M.warna
                    FROM CIMS.KOLEKSI K, CIMS.MATA M
                    WHERE K.id=M.id_koleksi;"""
            cursor.execute(query)
            result = namedtuplefetchall(cursor)
        except Exception as e:
            print(e)
        finally:
            cursor.close()
        
        return render(request, 'list_koleksi_mata.html', {'result': result, 'role': request.session['role']})

def list_koleksi_rumah(request):
    if 'username' not in request.session:
        return redirect('/login')

    with connection.cursor() as cursor:
        try:
            query = f"""SELECT R.id_koleksi, JB.nama, K.harga, JB.harga_beli, R.kapasitas_barang
                    FROM CIMS.KOLEKSI K, CIMS.RUMAH R, CIMS.KOLEKSI_JUAL_BELI JB
                    WHERE K.id=R.id_koleksi AND R.id_koleksi=JB.id_koleksi AND JB.id_koleksi=K.id;"""
            cursor.execute(query)
            result = namedtuplefetchall(cursor)
        except Exception as e:
            print(e)
        finally:
            cursor.close()
        
        return render(request, 'list_koleksi_rumah.html', {'result': result, 'role': request.session['role']})

def list_koleksi_barang(request):
    if 'username' not in request.session:
        return redirect('/login')

    with connection.cursor() as cursor:
        try:
            query = f"""SELECT B.id_koleksi, JB.nama, K.harga, JB.harga_beli, B.tingkat_energi
                    FROM CIMS.KOLEKSI K, CIMS.BARANG B, CIMS.KOLEKSI_JUAL_BELI JB
                    WHERE K.id=B.id_koleksi AND B.id_koleksi=JB.id_koleksi AND JB.id_koleksi=K.id;"""
            cursor.execute(query)
            result = namedtuplefetchall(cursor)
        except Exception as e:
            print(e)
        finally:
            cursor.close()
        
        return render(request, 'list_koleksi_barang.html', {'result': result, 'role': request.session['role']})

def list_koleksi_apparel(request):
    if 'username' not in request.session:
        return redirect('/login')

    with connection.cursor() as cursor:
        try:
            query = f"""SELECT A.id_koleksi, JB.nama, K.harga, JB.harga_beli, A.kategori_apparel, A.nama_pekerjaan FROM CIMS.KOLEKSI K, CIMS.APPAREL A, CIMS.KOLEKSI_JUAL_BELI JB
            WHERE K.id=A.id_koleksi AND A.id_koleksi=JB.id_koleksi AND JB.id_koleksi=K.id;"""
            cursor.execute(query)
            result = namedtuplefetchall(cursor)
        except Exception as e:
            print(e)
        finally:
            cursor.close()
        
        return render(request, 'list_koleksi_apparel.html', {'result': result, 'role': request.session['role']})

def menu_buat_koleksi(request):
    if 'username' not in request.session:
        return redirect('/login')

    return render(request, 'menu_buat_koleksi.html', {'role': request.session['role']})
