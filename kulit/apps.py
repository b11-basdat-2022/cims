from django.apps import AppConfig


class KulitConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'kulit'
