from django.shortcuts import render, redirect
from django.db import connection
from .forms import KulitForm
import re

# The "I am not racist" views for warna_kulit

def redirect_kulit(request):
    return redirect('/kulit/list')

def list_kulit(request):
    if 'username' not in request.session:
        return redirect('/login')

    response = { 'role': request.session['role'] }

    with connection.cursor() as cursor:
        cursor.execute("select kode from cims.warna_kulit")
        out = cursor.fetchall()
        response['kulit'] = url_friendly_kulit(out)

    return render(request, 'list_kulit.html', context = response)

def create_kulit(request):
    if 'username' not in request.session:
        return redirect('/login')

    if (request.session['role'] == 'pemain'):
        return redirect('/denied')

    response = { 'role': request.session['role'] }

    form = KulitForm(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        response['error_msg'] = check_valid_kulit(request.POST['kulit'])
        if response['error_msg'] == "":
            with connection.cursor() as cursor:
                query = f"""insert into cims.warna_kulit values
                ('{request.POST['kulit']}');"""
                cursor.execute(query)
            return redirect('/kulit/list')

    response['form'] = form

    return render(request, 'buat_kulit.html', context = response)

# This is a dangerous command
def delete_kulit(request):
    if 'username' not in request.session:
        return redirect('/login')

    if (request.session['role'] == 'pemain'):
        return redirect('/denied')

    response = { 'role': request.session['role'] }

    k = request.GET.get("k","")
    if (k != ""):
        response['chars'] = can_delete_kulit(k)
        if (len(response['chars']) == 0):
            destroy_kulit(k)
            return redirect('/kulit/list')
        else:
            return render(request, 'error_kulit.html', context = response)
    else:
        return redirect('/error')

# Stupid url doesn't recognize # as its own string.
# We have to convert it all to %23.
def url_friendly_kulit(kulit):
    out_url = []
    for kul in kulit:
        out_url.append((kul[0], kul[0].replace("#","%23")))
    return out_url

# We're going to use our friend regex for checking the right formatting
# WOAH COOL
def check_valid_kulit(kulit):
    is_right= re.search(r'^#(?:[0-9a-fA-F]{6})$', kulit)
    if is_right:                     
        return check_existing_kulit(kulit)
    else:
        return "Warna kulit tidak valid (format: #XXXXXX dimana X adalah angka hexadecimal [0-9,a-f,A-F])"

def check_existing_kulit(kulit):
    with connection.cursor() as cursor:
        query = f"""select kode from cims.warna_kulit
        where kode = '{kulit}';"""
        cursor.execute(query)
        out = cursor.fetchall()
        if (len(out) == 0):
            return ""
        else:
            return f"""Warna kulit '{kulit}' sudah terdaftar"""

def can_delete_kulit(kulit):
    with connection.cursor() as cursor:
        query = f"""select nama, username_pengguna from cims.tokoh
        where warna_kulit = '{kulit}';"""
        cursor.execute(query)
        out = cursor.fetchall()
        return out

def destroy_kulit(kulit):
    with connection.cursor() as cursor:
        query = f"""delete from cims.warna_kulit
        where kode = '{kulit}';"""
        cursor.execute(query)
    return
