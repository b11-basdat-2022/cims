from django.urls import path
from .views import redirect_kulit, list_kulit, create_kulit, delete_kulit

urlpatterns = [
    path('', redirect_kulit),
    path('list', list_kulit, name='list-kulit'),
    path('buat', create_kulit, name='buat-kulit'),
    path('hapus', delete_kulit, name='delete-kulit')
]
