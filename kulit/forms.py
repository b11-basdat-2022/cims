from django import forms

class KulitForm(forms.Form):
    kulit = forms.CharField(
        label = 'kulit',
        max_length = 7,
        required = True
    )
