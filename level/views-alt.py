from django.shortcuts import render, redirect
from django.db import connection
from .forms import LevelForm

# This is the least sense feature to implement due to lack of restriction:
#
# 1: There's no restriction for EXP adjustment.
# This means a level can have EXP requirement that is higher than the next level
# and the other way around 
# (e.g. Update level 2 to require 400 EXP while level 3 requires 340 EXP)
#
# 2: You can delete any level which means there can be a level gap
# (e.g. delete level 2 so there's only level 1 and 3)
#
# The simple solution is to give clarity for level
# or just rename the level system to rank system 
# like in the following page:
# https://bulbapedia.bulbagarden.net/wiki/Rank_(Mystery_Dungeon)
#
# But apparently, any logic and sense is not required
# as long as you get all the SQL stuff corret.

def redirect_level(request):
    return redirect('/level/list')

def list_level(request):
    if 'username' not in request.session:
        return redirect('/login')

    response = { 'role': request.session['role'] }

    with connection.cursor() as cursor:
        # Better to just order by xp requirement then
        cursor.execute("select * from cims.level order by xp asc;")
        out = cursor.fetchall()
        response['level'] = url_friendly_kulit(out)

    return render(request, 'list_level.html', context = response)

def create_level(request):
    if 'username' not in request.session:
        return redirect('/login')

    if (request.session['role'] == 'pemain'):
        return redirect('/denied')

    response = { 'role': request.session['role'] }

    form = LevelForm(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        if check_existing_level(request.POST['level'], 'level') == 0:
            with connection.cursor() as cursor:
                query = f"""insert into cims.level values
                ('{request.POST['level']}','{request.POST['exp']}');"""
                cursor.execute(query)
            return redirect('/kulit/list')
        else:
            response['error_msg'] = f"""Level {request.POST['level']} sudah terdaftar"""

    response['form'] = form

    return render(request, 'buat_level.html', context = response)

def update_level(request):
    if 'username' not in request.session:
        return redirect('/login')

    if (request.session['role'] == 'admin'):
        return redirect('/denied')

    l = request.GET.get("l", "")

    response = { 'role': request.session['role'] }
    if (l != ""):
        form = UpdateLevelForm(request.POST or None)
        if (request.method == 'POST' and form.is_valid()):
            set_level(l, request.POST['exp'])
            return redirect('/level/list')
    else:
        return redirect('/error')

    response['level'] = l
    response['form'] = form

    return render(request, 'update_level.html', context = response)

def delete_level(request):
    if 'username' not in request.session:
        return redirect('/login')

    if (request.session['role'] == 'pemain'):
        return redirect('/denied')

    response = { 'role': request.session['role'] }


# I just realize I can reuse this function
def check_existing_level(level, table):
    with connection.cursor() as cursor:
        query = f"""select level from cims.{table}
        where level = {level};"""
        cursor.execute(query)
        out = cursor.fetchall()
        # 0: Doesn't exist. Not 0: Exists. REMEMBER THIS!!!
        return len(out)

def set_level(level, exp):
    with connection.cursor() as cursor:
        query = f"""update cims.level
        set xp = {exp}
        where level = {level};"""
        cursor.execute(query)

