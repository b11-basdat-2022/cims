from django.urls import path
from .views import redirect_level, list_level, create_level, update_level, delete_level

urlpatterns = [
    path('', redirect_level),
    path('list', list_level, name='list-level'),
    path('buat', create_level, name='buat-level'),
    path('update', update_level, name='update-level'),
    path('hapus', delete_level, name='delete-level')
]
