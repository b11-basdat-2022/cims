from django.shortcuts import render, redirect
from django.db import connection
from .forms import LevelForm, UpdateLevelForm

# This is the least sense feature to implement due to lack of restriction:
#
# 1: There's no restriction for EXP adjustment.
# This means a level can have EXP requirement that is higher than the next level
# and the other way around 
# (e.g. Update level 2 to require 400 EXP while level 3 requires 340 EXP)
#
# 2: You can delete any level which means there can be a level gap
# (e.g. delete level 2 so there's only level 1 and 3)
#
# The simple solution is to give clarity for level
# or just rename the level system to rank system 
# like in the following page:
# https://bulbapedia.bulbagarden.net/wiki/Rank_(Mystery_Dungeon)
#
# But apparently, any logic and sense is not required
# as long as you get all the SQL stuff corret.

def redirect_level(request):
    return redirect('/level/list')

def list_level(request):
    if 'username' not in request.session:
        return redirect('/login')

    response = { 'role': request.session['role'] }

    with connection.cursor() as cursor:
        # Better to just order by xp requirement then
        cursor.execute("select * from cims.level where level != 1 order by xp asc;")
        response['level'] = cursor.fetchall()

    return render(request, 'list_level.html', context = response)

def create_level(request):
    if 'username' not in request.session:
        return redirect('/login')

    if (request.session['role'] == 'pemain'):
        return redirect('/denied')

    response = { 'role': request.session['role'] }

    form = LevelForm(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        response['error_msg'] = check_existing_level(request.POST['level'])
        if response['error_msg'] == "":
            with connection.cursor() as cursor:
                query = f"""insert into cims.level values
                ('{request.POST['level']}','{request.POST['exp']}');"""
                cursor.execute(query)
            return redirect('/level/list')

    response['form'] = form

    return render(request, 'buat_level.html', context = response)

def update_level(request):
    if 'username' not in request.session:
        return redirect('/login')

    if (request.session['role'] == 'pemain'):
        return redirect('/denied')

    l = request.GET.get("l", "")

    response = { 'role': request.session['role'] }
    if (l != "" and l != "1"):
        form = UpdateLevelForm(request.POST or None)
        if (request.method == 'POST' and form.is_valid()):
            set_level(l, request.POST['exp'])
            return redirect('/level/list')
    else:
        return redirect('/error')

    response['level'] = l
    response['form'] = form

    return render(request, 'update_level.html', context = response)

def delete_level(request):
    if 'username' not in request.session:
        return redirect('/login')

    if (request.session['role'] == 'pemain'):
        return redirect('/denied')

    response = { 'role': request.session['role'] }

    l = request.GET.get("l","")
    # DO NOT DELETE LEVEL 1 DO NOT DELETE LEVEL 1 DO NOT DELETE LEVEL 1
    if (l != "" and l != "1"):
        response['chars'] = can_delete_level(l)
        if (len(response['chars']) == 0):
            destroy_level(l)
            return redirect('/level/list')
        else:
            return render(request, 'error_level.html', context = response)
    else:
        return redirect('/error')

def check_existing_level(level):
    with connection.cursor() as cursor:
        query = f"""select level from cims.level
        where level = '{level}';"""
        cursor.execute(query)
        out = cursor.fetchall()
        if (len(out) == 0):
            return ""
        else:
            return f"""Level {level} sudah terdaftar"""

def set_level(level, exp):
    with connection.cursor() as cursor:
        query = f"""update cims.level
        set xp = {exp}
        where level = {level};"""
        cursor.execute(query)

def can_delete_level(level):
    with connection.cursor() as cursor:
        query = f"""select nama, username_pengguna from cims.tokoh
        where level = {level};"""
        cursor.execute(query)
        return cursor.fetchall()

def destroy_level(level):
    with connection.cursor() as cursor:
        query = f"""delete from cims.level
        where level = '{level}';"""
        cursor.execute(query)
