from django import forms

class LevelForm(forms.Form):
    level = forms.IntegerField(
        label = 'Level',
        required = True
    )
    exp = forms.IntegerField(
        label = 'EXP',
        required = True
    )

class UpdateLevelForm(forms.Form):
    exp = forms.IntegerField(
        label = 'New EXP',
        required = True
    )

