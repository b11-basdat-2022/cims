from django.shortcuts import render, redirect
from django.db import connection
from collections import namedtuple
from .forms import PekerjaanForm, UpdatePekerjaanForm

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def redirect_pekerjaan(request):
    return redirect('/pekerjaan/list')

def createpekerjaan(request):
    if 'username' not in request.session:
        return redirect('/login')

    if (request.session['role'] == 'pemain'):
        return redirect('/denied')

    response = { 'role': request.session['role'] }

    form = PekerjaanForm(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        response['error'] = check_existing_pekerjaan(request.POST['nama'])
        if response['error'] == "":
            with connection.cursor() as cursor:
                query = f"""INSERT INTO CIMS.PEKERJAAN VALUES
                ('{request.POST['nama']}','{request.POST['base_honor']}');"""
                cursor.execute(query)
            return redirect('/pekerjaan/list')

    response['form'] = form

    return render(request, 'createpekerjaan.html', context = response)

def readpekerjaan(request):
    if 'username' not in request.session:
        return redirect('/login')

    response = { 'role': request.session['role'] }

    cursor = connection.cursor()
    try:
        cursor.execute("SET SEARCH_PATH TO CIMS")
        cursor.execute("SELECT * FROM PEKERJAAN")
        response['pekerjaan'] = namedtuplefetchall(cursor)
        response['deletable'] = deletable()
    except Exception as e:
        print(e)
    finally:
        cursor.close()

    return render(request, 'listpekerjaan.html', context = response)

def updatepekerjaan(request):
    if 'username' not in request.session:
        return redirect('/login')

    if (request.session['role'] == 'pemain'):
        return redirect('/denied')
    
    response = { 'role': request.session['role'] }

    nama = request.GET.get("nama", "")

    if (nama != ""):
        form = UpdatePekerjaanForm(request.POST or None)
        if (request.method == 'POST' and form.is_valid()):
            with connection.cursor() as cursor:
                query = f"""UPDATE CIMS.PEKERJAAN
                SET base_honor = {request.POST['new_base_honor']}
                WHERE nama = '{nama}';"""
                cursor.execute(query)
            return redirect('/pekerjaan/list')
    else:
        return redirect('/error')

    response['nama'] = nama
    response['form'] = form

    return render(request, 'updatepekerjaan.html', context = response)

def deletepekerjaan(request):
    if 'username' not in request.session:
        return redirect('/login')

    if (request.session['role'] == 'pemain'):
        return redirect('/denied')

    nama = request.GET.get("nama","")

    if (nama in deletable()):
        with connection.cursor() as cursor:
            query = f"""DELETE FROM CIMS.PEKERJAAN
            WHERE nama = '{nama}';"""
            cursor.execute(query)
        return redirect('/pekerjaan/list')
    else:
        return redirect('/error')

def check_existing_pekerjaan(nama):
    with connection.cursor() as cursor:
        query = f"""SELECT nama FROM CIMS.PEKERJAAN
        WHERE nama = '{nama}';"""
        cursor.execute(query)
        out = cursor.fetchall()

        if (len(out) == 0):
            return ""
        else:
            return f"""Pekerjaan dengan nama "{nama}" sudah terdaftar"""

def deletable():
    with connection.cursor() as cursor:
        query = f"""SELECT P.nama FROM PEKERJAAN P
        WHERE NOT EXISTS (SELECT * FROM TOKOH T WHERE T.pekerjaan=P.nama) and
        NOT EXISTS (SELECT * FROM BEKERJA B WHERE B.nama_pekerjaan=P.nama) and
        NOT EXISTS (SELECT * FROM APPAREL A WHERE A.nama_pekerjaan=P.nama)"""
        cursor.execute("SET SEARCH_PATH TO CIMS")
        cursor.execute(query)
        out = cursor.fetchall()
        return list(sum(out, ()))