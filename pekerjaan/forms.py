from django import forms

class PekerjaanForm(forms.Form):
    nama = forms.CharField(
        label = 'Nama Pekerjaan',
        required = True
    )
    base_honor = forms.IntegerField(
        label = 'Base Honor',
        required = True
    )

class UpdatePekerjaanForm(forms.Form):
    new_base_honor = forms.IntegerField(
        label = 'Base Salary',
        required = True
    )