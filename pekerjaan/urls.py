from django.urls import path
from . import views

app_name = 'pekerjaan'

urlpatterns = [
    path('', views.redirect_pekerjaan, name='redirectpekerjaan'),
    path('create', views.createpekerjaan, name='createpekerjaan'),
    path('list', views.readpekerjaan, name='listpekerjaan'),
    path('update', views.updatepekerjaan, name='updatepekerjaan'),
    path('delete', views.deletepekerjaan, name='deletepekerjaan')
]
