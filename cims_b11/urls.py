"""cims_b11 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, re_path, include
from dummy.views import dummy, access_denied, error_generic
from django.contrib import admin
from home.views import index as index_home
from login.views import login, logout
import signup.urls as signup
import tokoh.urls as tokoh
import kulit.urls as kulit
import level.urls as level
import misi.urls as misi
import makanan.urls as makanan
import bekerja.urls as bekerja
import menggunakan_barang.urls as menggunakan_barang
import pekerjaan.urls as pekerjaan
import kategori_apparel.urls as kategoriApparel
import koleksi.urls as koleksi
import koleksi_tokoh.urls as koleksiTokoh
import use_apparel.urls as use_apparel
import makan.urls as makan

urlpatterns = [
    path('admin/', admin.site.urls),
    path('dummy/', dummy, name='dummy'),
    path('error/', error_generic, name='error'),
    path('denied/', access_denied, name='denied'),
    path('', index_home, name='index'),
    path('login/', login, name='login'),
    path('logout/', logout, name='logout'),
    path('signup/', include(signup)),
    path('tokoh/', include(tokoh)),
    path('kulit/', include(kulit)),
    path('level/', include(level)),
    path('misi/', include(misi)),
    path('bekerja/', include(bekerja)),
    path('menggunakan-barang/', include(menggunakan_barang)),
    path('pekerjaan/', include(pekerjaan)),
    path('kategori-apparel/', include(kategoriApparel)),
    path('koleksi/', include(koleksi)),
    path('koleksi-tokoh/', include(koleksiTokoh)),
    path('menggunakan-apparel/', include(use_apparel)),
    path('makan/', include(makan)),
    path('makanan/', include(makanan))
]
