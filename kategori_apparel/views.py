import re
from urllib import response
from django.shortcuts import redirect, render
from django.http.response import HttpResponse
from django.core import serializers
from django.db import connection, IntegrityError
from collections import namedtuple
from django.contrib import messages
from .forms import CreateKategoriApparel

# Create your views here.

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def read_kategori_apparel(request):
    if 'username' not in request.session:
        return redirect('/login')

    with connection.cursor() as cursor:
        try:
            query = f"""SELECT nama_kategori, CASE WHEN nama_kategori IN (SELECT kategori_apparel FROM CIMS.APPAREL)
            THEN FALSE
            ELSE TRUE
            END AS deleteable
            FROM CIMS.KATEGORI_APPAREL;"""
            cursor.execute(query)
            result = namedtuplefetchall(cursor)
        except Exception as e:
            print(e)
        finally:
            cursor.close()

        return render(request, 'list_kategori_apparel.html', {'result': result, 'role': request.session['role']})

def create_kategori_apparel(request):
    if 'username' not in request.session:
        return redirect('/login')

    if (request.session['role'] == 'pemain'):
        return redirect('/denied')

    with connection.cursor() as cursor:
        if request.method == 'POST':
            form = CreateKategoriApparel(request.POST)
            if form.is_valid():
                try:
                    nama_kategori = form.cleaned_data['nama_kategori']
                    cursor.execute("INSERT INTO CIMS.KATEGORI_APPAREL VALUES (%s)",[nama_kategori])
                    return redirect('/kategori-apparel/daftar-kategori-apparel')
                except IntegrityError:
                    messages.info(request, "Error: Data tidak boleh duplikat")
                    return redirect('/kategori-apparel/create-kategori-apparel')
        else :
            form = CreateKategoriApparel()
            return render(request, 'create_kategori_apparel.html', {'form' : form, 'role': request.session['role']})

def delete_kategori_apparel(request, key):
    if 'username' not in request.session:
        return redirect('/login')

    if (request.session['role'] == 'pemain'):
        return redirect('/denied')

    with connection.cursor() as cursor:
        try:
            cursor.execute("DELETE FROM CIMS.KATEGORI_APPAREL WHERE nama_kategori = %s", [key])
            return redirect('/kategori-apparel/daftar-kategori-apparel')
        except Exception as e:
            print(e)
        finally:
            cursor.close()
    
