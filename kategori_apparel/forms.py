from django import forms

class CreateKategoriApparel(forms.Form):
    nama_kategori = forms.CharField(
        label = 'Nama Kategori',
        max_length = 10,
        required = True
    )
