from django.urls import path
from .views import read_kategori_apparel, create_kategori_apparel, delete_kategori_apparel

urlpatterns = [
    path('daftar-kategori-apparel', read_kategori_apparel, name='daftar-kategori-apparel'),
    path('create-kategori-apparel', create_kategori_apparel, name='create-kategori-apparel'),
    path('delete-kategori-apparel/<str:key>', delete_kategori_apparel, name='delete-kategori-apparel')
]
