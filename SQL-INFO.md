# Semua views dan stored procedure
Markdown ini dipergunakan untuk mendokumentasi segala tambahan views dan stored procedure yang kami buat saat mengembangkan aplikasi web "The Cims."

## Views
### Akun_Pass
View ini digunakan untuk menyatukan password dari semua akun yang terdaftar. Tabel password untuk admin dan pemain dapat kita gunakan untuk menentukan role akun dengan mudah. Penggunaan view ini dapat dilihat di [views login](login/views.py).
```SQL
create view akun_pass as
select k.username, a.password as admin_pass, p.password as pemain_pass
from akun k
left outer join admin a on a.username = k.username
left outer join pemain p on p.username = k.username;
```
### Mengapp
Kepanjangan dari **Meng**gunakan **App**arel, view ini digunakan untuk mengembalikan tabel sesuai pada deskripsi aplikasi untuk daftar penggunaan apparel. Penggunaan view ini dapat dilihat di [views use_apparel](use_apparel/views.py)
```SQL
create view mengapp as
select m.username_pengguna, m.nama_tokoh, jb.nama, 
a.warna_apparel, coalesce(a.nama_pekerjaan, 'tidak ada'),
a.kategori_apparel, m.id_koleksi
from cims.menggunakan_apparel m
join cims.apparel a on m.id_koleksi = a.id_koleksi
join cims.koleksi_jual_beli jb on m.id_koleksi = jb.id_koleksi;
```

## Stored Procedure

### Nomor 1 (Putih)
**Nama**: `autoadd_koleksi()`  
**Trigger**: `autoadd_koleksi_trigger`  
**Perilaku**: Menambah 3 baris untuk tabel `cims.koleksi_tokoh` dari setiap baris username dan tokoh yang ditambah. ID Koleksi yang ditambahkan selalu `rb001`, `mt001`, dan `rm001`.  

**SQL**:  
```SQL
create or replace function autoadd_koleksi()
returns trigger as
$$
    begin
        insert into cims.koleksi_tokoh values
        ('rb001', new.username_pengguna, new.nama),
        ('mt001', new.username_pengguna, new.nama),
        ('rm001', new.username_pengguna, new.nama);

        return new;
    end;
$$
language plpgsql;

create trigger autoadd_koleksi_trigger
after insert on cims.tokoh
for each row
execute procedure autoadd_koleksi();
```

### Nomor 2 (Hijau)
**Nama**:  koin_xp_update_bekerja()
**Trigger**: trigger_koin_xp_update_bekerja
**Perilaku**:  Mengupdate koin dan exp setelah insert data pada tabel BEKERJA

**SQL**:
```SQL
CREATE OR REPLACE FUNCTION koin_xp_update_bekerja()
RETURNS trigger AS
$$
    DECLARE
        apparel_cocok INTEGER;
	new_honor INTEGER;
    BEGIN
        -- Sifat cocok dengan pekerjaan
	SET SEARCH_PATH TO CIMS;
        IF EXISTS (SELECT T.username_pengguna FROM TOKOH T, PEKERJAAN_COCOK PC
	WHERE T.username_pengguna=NEW.username_pengguna AND
	T.nama=NEW.nama_tokoh AND
	T.pekerjaan=PC.nama_pekerjaan AND
	T.sifat=PC.nama_sifat)
	THEN
	UPDATE PEMAIN
        SET koin = koin + 10
        WHERE username = NEW.username_pengguna;
	END IF;

	-- Apparel sesuai dengan pekerjaan
	SELECT COUNT(*) into apparel_cocok
	FROM MENGGUNAKAN_APPAREL MA, APPAREL A
	WHERE MA.username_pengguna=NEW.username_pengguna AND
	MA.nama_tokoh=NEW.nama_tokoh AND
	A.id_koleksi=MA.id_koleksi AND
	A.nama_pekerjaan=NEW.nama_pekerjaan;

	UPDATE TOKOH T
        SET xp = xp + apparel_cocok*5
        WHERE T.username_pengguna = NEW.username_pengguna AND
	T.nama=NEW.nama_tokoh;
	
	-- Menghitung honor
	SELECT P.base_honor*T.level AS honor into new_honor
	FROM TOKOH T, PEKERJAAN P
	WHERE T.username_pengguna=NEW.username_pengguna AND
	T.nama=NEW.nama_tokoh AND
	T.pekerjaan=P.nama;
	
        UPDATE PEMAIN
        SET koin = koin + new_honor
        WHERE username = NEW.username_pengguna;
        RETURN NEW;
    END;
$$
LANGUAGE plpgsql;

CREATE TRIGGER trigger_koin_xp_update_bekerja
AFTER INSERT ON CIMS.BEKERJA
FOR EACH ROW
EXECUTE PROCEDURE koin_xp_update_bekerja();
```

### Nomor 3 (Kuning)
**Nama**: `replace_apparel()`  
**Trigger**: `replace_apparel_trigger`  
**Perilaku**: Menggantikan baris dari cims.menggunakan_apparel ketika ada penambahan baris baru yang memiliki id apparel dengan kategori yang sama dari suatu id apprel yang sudah ada dalam tabel untuk tokoh yang sama. Informasi penggunaan id apparel lama tersebut akan dihapus dan diganti dengan yang baru.

```SQL
create or replace function replace_apparel()
returns trigger as
$$
    declare
        kat varchar(10);
        id_ap varchar(5);
    begin
        -- Find the category for added apparel
        select kategori_apparel into kat
        from cims.apparel 
        where id_koleksi = new.id_koleksi;

        -- Then we find the existing apparel id a tokoh is wearing
        -- that has the same category
        select id_koleksi into id_ap
        from mengapp m
        where m.username_pengguna = new.username_pengguna
        and m.nama_tokoh = new.nama_tokoh
        and m.kategori_apparel = kat
        and m.id_koleksi != new.id_koleksi;

        -- Lastly, kill it from cims.menggunakan_apparel
        -- If id_app returns nothing, it can be ignored safely.
        delete from cims.menggunakan_apparel m
        where m.username_pengguna = new.username_pengguna
        and m.nama_tokoh = new.nama_tokoh
        and m.id_koleksi = id_ap;

        return new;
    end;
$$
language plpgsql;

create trigger replace_apparel_trigger
after insert on cims.menggunakan_apparel
for each row
execute procedure replace_apparel();
```

### Nomor 4 (Ungu)
**Nama**:  
**Trigger**:  
**Perilaku**:  

**SQL**:
```SQL
\d
```

### Nomor 5 (Biru)
**Nama**:  
**Trigger**:  
**Perilaku**:  

**SQL**:
```SQL
\d
```
