from django.urls import path
from .views import *

urlpatterns = [
    path('list-makanan', makanan, name='read-makanan'),
    path('create-makanan', create_makanan, name='create-makanan'),
    path('delete-makanan/<str:nama>/', delete_makanan, name='delete-makanan'),
    path('update-makanan/<str:nama>/', update_makanan, name='update-makanan')
]
