from django.shortcuts import render, redirect
from django.http.response import HttpResponse
from django.core import serializers
from django.contrib import messages

# Create your views here.
from django.db import connection
from collections import namedtuple

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def makanan(request):
    if 'username' not in request.session:
        return redirect('/')

    with connection.cursor() as cursor:
        result = None
        try:
            cursor.execute(f"""
                            SELECT nama, harga, tingkat_energi,
                            tingkat_kelaparan, count(nama_tokoh) AS jumlah_dimakan
                            FROM CIMS.MAKANAN
                            LEFT OUTER JOIN CIMS.MAKAN
                            ON nama = nama_makanan
                            GROUP BY nama, harga, tingkat_energi, tingkat_kelaparan
                            ;
                            """)
            result = namedtuplefetchall(cursor)
        except Exception as e:
            print(e)
        finally:
            cursor.close()

        return render(request, 'list_makanan.html', {'result': result, 'role': request.session['role']})


def check_data(data):
    if (data is None) or data == '':
        return False
    else:
        return True

def delete_makanan(request, nama):
    if 'username' not in request.session:
        return redirect('/login')

    with connection.cursor() as cursor:
        result = None
        try:
            query = f"""
                     DELETE FROM CIMS.MAKANAN
                     WHERE nama = '{nama}';
                     """
            cursor.execute(query)

        except Exception as e:
            print(e)
        finally:
            cursor.close()

        return redirect(makanan)

def update_makanan(request, nama):
    if 'username' not in request.session:
        return redirect('/login')

    with connection.cursor() as cursor:
        result = None
        try:
            if request.method == 'POST':
                #I need to complete this part :v
                nama = request.POST['nama']
                harga = request.POST['harga']
                tingkat_energi = request.POST['tingkat_energi']
                tingkat_kelaparan = request.POST['tingkat_kelaparan']
                if (check_data(nama) and
                    check_data(harga) and
                    check_data(tingkat_energi) and
                    check_data(tingkat_kelaparan)) == False:
                    messages.info(request, 'Data insufficient')
                else:
                    query = f"""
                                UPDATE CIMS.MAKANAN
                                SET harga = {harga},
                                tingkat_energi = {tingkat_energi},
                                tingkat_kelaparan = {tingkat_kelaparan}

                                WHERE nama = '{nama}';
                            """


                    cursor.execute(query)
                    return redirect(makanan)

            if request.session['role'] == 'pemain':
                return redirect(makanan)

            query = f"""
                        SELECT * FROM CIMS.MAKANAN
                        WHERE nama = '{nama}'
                    """
            cursor.execute(query)
            result = namedtuplefetchall(cursor)

        except Exception as e:
            print(e)
        finally:
            cursor.close()

        return render(request, 'update_makanan.html', {'result': result, 'role': request.session['role']})


def create_makanan(request):
    if 'username' not in request.session:
        return redirect('/login')

    with connection.cursor() as cursor:
        result = None
        try:
            if request.method == 'POST':
                #I need to complete this part :v
                nama = request.POST['nama']
                harga = request.POST['harga']
                tingkat_energi = request.POST['tingkat_energi']
                tingkat_kelaparan = request.POST['tingkat_kelaparan']
                if (check_data(nama) and
                    check_data(harga) and
                    check_data(tingkat_energi) and
                    check_data(tingkat_kelaparan)) == False:
                    messages.info(request, 'Data insufficient')
                else:
                    query = f"""
                                INSERT INTO CIMS.MAKANAN (nama,
                                harga,
                                tingkat_energi,
                                tingkat_kelaparan)
                                VALUES ('{nama}',
                                {harga},
                                {tingkat_energi},
                                {tingkat_kelaparan});
                            """

                    cursor.execute(query)
                    return redirect(makanan)

            if request.session['role'] == 'pemain':
                return redirect(makanan)

        except Exception as e:
            print(e)
        finally:
            cursor.close()

        return render(request, 'create_makanan.html', {'result': result, 'role': request.session['role']})
