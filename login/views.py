from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.db import connection
from .forms import LoginForm


# The place where we can login our user
def login(request):

    if 'username' in request.session:
        return redirect('/')

    response = {}

    form = LoginForm(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        response['username'] = request.POST['username']
        response['password'] = request.POST['password']
        response['error_msg'] = ""
        test = check_register(response['username'], response['password'])
        
        if test == 0:
            response['error_msg'] = "Login gagal (username/password salah atau user belum ada)"
        else:
            request.session['is_login'] = True
            request.session['username'] = response['username']
            request.session['password'] = response['password']
            request.session['is_admin'] = True if test == 1 else False
            request.session['role'] = "admin" if test == 1 else "pemain"
            return redirect('/')

    response['form'] = form

    return render(request, 'login.html', context = response)

# un: username, pw: password
# 0: Doesn't exist, 1: Admin, 2: Pemain
def check_register(un, pw):
    with connection.cursor() as cursor:
        # To simplify our life, I've added a view 'akun_pass' to the following:
        # create view akun_pass as
        # select k.username, a.password as admin_pass, p.password as pemain_pass
        # from akun k
        # left outer join admin a on a.username = k.username
        # left outer join pemain p on p.username = k.username;
        cursor.execute("set search_path to cims;")
        query = f"""select * from akun_pass 
        where username = '{un}'
        and admin_pass = '{pw}'
        or pemain_pass = '{pw}';"""
        cursor.execute(query)
        out = cursor.fetchall()
        cursor.execute("set search_path to public;")

        if (len(out) == 0):
            return 0
        else:
            print(out)
            if out[0][2] == None:
                return 1
            else:
                return 2


def logout(request):
    if 'username' not in request.session:
        return redirect('/')

    request.session.flush()
    return redirect('/')
