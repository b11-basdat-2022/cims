from django.shortcuts import render
from django.db import connection

def index(request):
    response = {}
    if 'username' in request.session:
        response = {
                'name': request.session['username'],
                'role': request.session['role']
            }
        if (response['role'] == 'pemain'):
            with connection.cursor() as cursor:
                query = f"""select email, no_hp, koin
                from cims.pemain
                where username = '{response['name']}'"""
                cursor.execute(query)
                out = cursor.fetchall()
                print(out)
                response['email'] = out[0][0]
                response['nohp'] = out[0][1]
                response['koin'] = out[0][2]

    return render(request, 'index.html', context = response)
