from django.apps import AppConfig


class UseApparelConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'use_apparel'
