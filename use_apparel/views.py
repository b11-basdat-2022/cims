from django.shortcuts import render, redirect
from django.db import connection
from .forms import MengappForm
import json

# I've added view "mengap" to the following:
# create view mengapp as
# select m.username_pengguna, m.nama_tokoh, jb.nama, 
# a.warna_apparel, coalesce(a.nama_pekerjaan, 'tidak ada'),
# a.kategori_apparel, m.id_koleksi
# from cims.menggunakan_apparel m
# join cims.apparel a on m.id_koleksi = a.id_koleksi
# join cims.koleksi_jual_beli jb on m.id_koleksi = jb.id_koleksi;

# tokoh_choice = [] # In choice
# apparel_dict = {} # In str: list
# apparel_all_choice = [] # In choice

def redirect_use_apparel(request):
    return redirect('/menggunakan-apparel/list')

def list_use_apparel(request):
    if 'username' not in request.session:
        return redirect('/login')

    response = { 
            'name': request.session['username'],
            'role': request.session['role'],
            }

    with connection.cursor() as cursor:
        if response['role'] == 'pemain':
            query = f"""select * from cims.mengapp
            where username_pengguna = '{response['name']}';"""
        else:
            query = "select * from cims.mengapp;"
        cursor.execute(query)
        response['chars'] = cursor.fetchall()

    return render(request, 'list_mengapp.html', context = response)

def create_use_apparel(request):
    if 'username' not in request.session:
        return redirect('/login')

    if (request.session['role'] == 'admin'):
        return redirect('/denied')

    response = { 
            'role': request.session['role'],
            'username': request.session['username'] 
            }

    tl, tc, ad, alc = gen_own_tokoh(response['username'])
        
    form = MengappForm(tc, alc, request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        if 'apparel' in request.POST:
            u = response['username']
            t = request.POST['tokoh']
            a = request.POST['apparel'] 
            response['error_msg'] = can_wear_apparel(u, t, a)
            if response['error_msg'] == "":
                add_use_apparel(u, t, a)
                return redirect('/menggunakan-apparel/list')
        else:
            response['error_msg'] = "Apparel tidak boleh kosong"

    # JSON Stuff
    response['form'] = form
    response['json_tokoh_list'] = json.dumps(tl)
    response['json_tokoh_apparel'] = json.dumps(ad)

    return render(request, 'buat_mengapp.html', context = response)

def delete_use_apparel(request):
    if 'username' not in request.session:
        return redirect('/login')

    if (request.session['role'] == 'admin'):
        return redirect('/denied')

    response = { 'role': request.session['role'] }

    t = request.GET.get("t","")
    ap = request.GET.get("ap","")
    # Oh wow, we don't have to check for anything :)
    if (t != "" and ap != ""):
        destroy_use_apparel(request.session['username'], t, ap)
        return redirect('/menggunakan-apparel/list')
    else:
        return redirect('/error')

def gen_own_tokoh(username):
    tokoh_list = [] # In list
    tokoh_choice = [(None,"---")] # In choice
    apparel_dict = {} # In str: list
    apparel_all_choice = [] # In choice

    with connection.cursor() as cursor:
        # Get all tokoh from tokoh table
        query = f"""select nama from cims.tokoh
        where username_pengguna = '{username}';"""
        cursor.execute(query)
        temp_list = list_tuple_convert(cursor.fetchall())
        tokoh_list.extend(temp_list[0])
        tokoh_choice.extend(temp_list[1])
        
        # Then get each tokoh's apparel
        for tokoh in tokoh_choice:
            temptok = tokoh[0] # Not Tiktok shutup
            query = f"""select id_koleksi from cims.koleksi_tokoh
            where id_koleksi like 'ap%'
            and username_pengguna = '{username}'
            and nama_tokoh = '{temptok}';"""
            cursor.execute(query)
            temp_list = list_tuple_convert(cursor.fetchall())
            apparel_dict[temptok] = temp_list[0]
            apparel_all_choice.extend(temp_list[1])

    return tokoh_list, tokoh_choice, apparel_dict, apparel_all_choice

def list_tuple_convert(in_list):
    out_list = [[],[]]
    for row in in_list:
        out_list[0].append(row[0])
        out_list[1].append((row[0], row[0]))
    return out_list

def can_wear_apparel(username, tokoh, apparel):
    with connection.cursor() as cursor:
        query = f"""select * from cims.menggunakan_apparel
        where username_pengguna = '{username}'
        and nama_tokoh = '{tokoh}'
        and id_koleksi = '{apparel}';"""
        cursor.execute(query)
        if len(cursor.fetchall()) == 0:
            return ""
        else:
            return f"""{tokoh} sudah menggunakan apparel id '{apparel}'"""

def add_use_apparel(username, tokoh, apparel):
    with connection.cursor() as cursor:
        query = f"""insert into cims.menggunakan_apparel values
        ('{username}', '{tokoh}', '{apparel}');"""
        cursor.execute(query)

def destroy_use_apparel(username, tokoh, apparel):
    with connection.cursor() as cursor:
        query = f"""delete from cims.menggunakan_apparel
        where username_pengguna = '{username}'
        and nama_tokoh = '{tokoh}'
        and id_koleksi = '{apparel}';"""
        cursor.execute(query)
