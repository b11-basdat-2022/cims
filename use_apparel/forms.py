from django import forms

class MengappForm(forms.Form):
    def __init__(self, tokoh, apparel, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['tokoh'] = forms.CharField(
            label = 'Nama Tokoh',
            required = True,
            widget = forms.Select(choices = tokoh)
        )
        self.fields['apparel'] = forms.CharField(
            label = 'Apparel',
            required = False, # This will be handled manually
            widget = forms.Select(choices = apparel)
        )

