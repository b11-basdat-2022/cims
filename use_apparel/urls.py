from django.urls import path
from .views import redirect_use_apparel, list_use_apparel, create_use_apparel, delete_use_apparel

urlpatterns = [
    path('', redirect_use_apparel),
    path('list', list_use_apparel, name='list-menggunakan-apparel'),
    path('buat', create_use_apparel, name='buat-menggunakan-apparel'),
    path('hapus', delete_use_apparel, name='delete-menggunakan-apparel')
]
