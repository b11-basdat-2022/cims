from django.shortcuts import render, redirect
from django.http.response import HttpResponse
from django.core import serializers
from django.contrib import messages

# Create your views here.
from django.db import connection
from collections import namedtuple

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def read_makan(request):
    if 'username' not in request.session:
        return redirect('/login')

    with connection.cursor() as cursor:
        result = None
        try:
            if request.session['role'] == 'pemain':
                query = f"""SELECT * FROM CIMS.MAKAN WHERE username_pengguna = '{request.session['username']}';"""
            else:
                query = "SELECT * FROM CIMS.MAKAN;"
            cursor.execute(query)
            result = namedtuplefetchall(cursor)
        except Exception as e:
            print(e)
        finally:
            cursor.close()

        if result != None:
            return render(request, 'read_makan.html', {'result': result, 'role': request.session['role']})
        else:
            return render(request, 'read_makan.html', {'role': request.session['role']})


def create_makan(request):
    if 'username' not in request.session:
        return redirect('/login')

    with connection.cursor() as cursor:
        result = None
        tokoh = None
        makanan = None
        try:
            if request.method == 'POST':
                #I need to complete this part :v
                nama_tokoh = request.POST['tokoh']
                nama_makanan = request.POST['makanan']
                if nama_tokoh == " " or nama_makanan == " ":
                    messages.info(request, 'Data insufficient')
                else:
                    #insert and stuff should be here
                    return redirect(read_makan)

            if request.session['role'] == 'admin':
                return redirect(read_makan)

            query = f"""SELECT nama FROM CIMS.TOKOH WHERE username_pengguna = '{request.session['username']}';"""
            cursor.execute(query)
            tokoh = namedtuplefetchall(cursor)

            query = f"""SELECT nama FROM CIMS.MAKANAN;"""
            cursor.execute(query)
            makanan = namedtuplefetchall(cursor)
        except Exception as e:
            print(e)
        finally:
            cursor.close()

        return render(request, 'create_makan.html', {'result': result, 'tokoh': tokoh, 'makanan': makanan, 'role': request.session['role']})
