from django.urls import path
from .views import *

urlpatterns = [
    path('list-makan', read_makan, name='read-makan'),
    path('create-makan', create_makan, name='create-makan')
]
