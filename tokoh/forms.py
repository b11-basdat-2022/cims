from django import forms
from django.db import connection

# I put more options for da memez
kelamin_choice = [
        ('Laki-laki', 'Laki-laki'),
        ('Perempuan', 'Perempuan'),
        ('Lainnya', 'Lainnya'),
        ('Helikopter', 'Helikopter'),
        ]
temp_choice = {}
koleksi_choice = {}

def list_tuple_to_choice(in_list):
    out_list = []
    for row in in_list:
        out_list.append((row[0], row[0]))
    return out_list

# Regen every option everythime we open this form
# This is important when we add more warna_kulit, sifat, and pekerjaan.
# I'm too lazy to implement a listerner feature
def gen_all_tokoh():
    global temp_choice
    temp_choice.clear()

    with connection.cursor() as cursor:
        query = "select kode from cims.warna_kulit;"
        cursor.execute(query)
        temp_choice["kulit"] = list_tuple_to_choice(cursor.fetchall())

        query = "select nama_sifat from cims.sifat;"
        cursor.execute(query)
        temp_choice["sifat"] = list_tuple_to_choice(cursor.fetchall())

        query = "select nama from cims.pekerjaan;"
        cursor.execute(query)
        temp_kerja = list_tuple_to_choice(cursor.fetchall())
        temp_kerja.append(("null", "tidak ada"))
        temp_choice["kerja"] = temp_kerja
    return

def gen_koleksi_tokoh(username, tokoh):
    global koleksi_choice
    koleksi_choice = {
        'rb': [('rb001','rb001')],
        'mt': [('mt001','mt001')],
        'rm': [('rm001','rm001')]
        }

    with connection.cursor() as cursor:
        query = f"""select id_koleksi
        from cims.koleksi_tokoh
        where username_pengguna = '{username}'
        and nama_tokoh = '{tokoh}';"""
        cursor.execute(query)

        for id_get in list_tuple_to_choice(cursor.fetchall()):
            if "rb" in id_get[0]:
                koleksi_choice['rb'].append(id_get)
            elif "mt" in id_get[0]:
                koleksi_choice['mt'].append(id_get)
            elif "rm" in id_get[0]:
                koleksi_choice['rm'].append(id_get)
            # Anything else like apXXX are ignored
    return


# For CreateTokohForm, always run get_kulit first
def get_kulit():
    gen_all_tokoh()
    return temp_choice["kulit"]

def get_sifat():
    return temp_choice["sifat"]

def get_pekerjaan():
    return temp_choice["kerja"]

# For UpdateTokohForm, Always run get_rambut first
def get_rambut(username, tokoh):
    gen_koleksi_tokoh(username, tokoh)
    return koleksi_choice['rb']

def get_mata():
    return koleksi_choice['mt']

def get_rumah():
    return koleksi_choice['rm']

class CreateTokohForm(forms.Form):
    tokoh_name = forms.CharField(
        label = 'Nama Tokoh',
        max_length = 50,
        required = True
    )
    kelamin = forms.CharField(
        label = 'Kelamin',
        required = True,
        widget = forms.Select(choices = kelamin_choice)
    )
    kulit = forms.CharField(
        label = 'Kulit',
        required = True,
        widget = forms.Select(choices = get_kulit())
    )
    sifat = forms.CharField(
        label = 'Sifat',
        required = True,
        widget = forms.Select(choices = get_sifat())
    )
    pekerjaan = forms.CharField(
        label = 'Pekerjaan',
        required = True,
        widget = forms.Select(choices = get_pekerjaan())
    )

# If you need your form to take argument, you need to use def __init__
class UpdateTokohForm(forms.Form):
    def __init__(self, username, tokoh, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['rambut'] = forms.CharField(
                label = "ID_Rambut", 
                required = True,
                widget = forms.Select(choices = get_rambut(username, tokoh))
        )
        self.fields['mata'] = forms.CharField(
                label = "ID_Mata", 
                required = True,
                widget = forms.Select(choices = get_mata())
        )
        self.fields['rumah'] = forms.CharField(
                label = "ID_Rumah", 
                required = True,
                widget = forms.Select(choices = get_rumah())
        )

