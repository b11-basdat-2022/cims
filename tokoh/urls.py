from django.urls import path
from .views import redirect_tokoh, list_tokoh, detail_tokoh, create_tokoh, update_tokoh

urlpatterns = [
    path('', redirect_tokoh),
    path('list', list_tokoh, name='list-tokoh'),
    path('detail', detail_tokoh, name='detail-tokoh'),
    path('buat', create_tokoh, name='buat-tokoh'),
    path('update', update_tokoh, name='update-tokoh')
]
