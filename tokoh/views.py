from django.shortcuts import render, redirect
from django.db import connection
from .forms import CreateTokohForm, UpdateTokohForm

# Create your views here.
def redirect_tokoh(request):
    return redirect('/tokoh/list')

def list_tokoh(request):
    if 'username' not in request.session:
        return redirect('/login')

    response = { 
            'name': request.session['username'],
            'role': request.session['role'],
            }

    if request.session['role'] == 'pemain':
        with connection.cursor() as cursor:
            query = f"""select username_pengguna, nama, jenis_kelamin,
            status, xp, energi, kelaparan, hubungan_sosial, level, sifat
            from cims.tokoh where 
            username_pengguna = '{request.session['username']}';"""
            cursor.execute(query)
            out = cursor.fetchall()
            response['chars'] = out
            return render(request, 'list_tokoh.html', context = response)
            
    if request.session['role'] == 'admin':
        with connection.cursor() as cursor:
            query = f"""select username_pengguna, nama, jenis_kelamin,
            status, xp, energi, kelaparan, hubungan_sosial, level, sifat
            from cims.tokoh;"""
            cursor.execute(query)
            out = cursor.fetchall()
            response['chars'] = out
            return render(request, 'list_tokoh.html', context = response)

    return redirect('/')

def detail_tokoh(request):
    if 'username' not in request.session:
        return redirect('/login')

    u = request.GET.get("u", "")
    t = request.GET.get("t", "")
    r = request.session['role']

    response = {'is_allowed': False, 'role': r}
    # If the user is pemain, also check if the u query matches with the current username
    if ((u != "" and t != "") 
            and ((r == "pemain" and u == request.session['username'])
            or (r == "admin"))):

        with connection.cursor() as cursor:
            query = f"""select nama, username_pengguna, id_rambut, id_mata,
            id_rumah, warna_kulit, coalesce(pekerjaan, 'tidak ada')
            from cims.tokoh
            where username_pengguna = '{u}'
            and nama = '{t}';"""
            cursor.execute(query)
            out = cursor.fetchall()
            if (len(out) != 0):
                response['infos'] = out
                response['is_allowed'] = True


    return render(request, 'detail_tokoh.html', context = response)

def create_tokoh(request):
    if 'username' not in request.session:
        return redirect('/login')

    if (request.session['role'] == 'admin'):
        return redirect('/denied')

    response = {}
    form = CreateTokohForm(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        response['nama_tokoh'] = request.POST['tokoh_name']
        response['error_msg'] = check_name_dups(
                request.session['username'], response['nama_tokoh'])
        if response['error_msg'] == "":
            response['kelamin'] = request.POST['kelamin']
            response['kulit'] = request.POST['kulit']
            response['sifat'] = request.POST['sifat']
            response['pekerjaan'] = request.POST['pekerjaan']
            if response['pekerjaan'] != "null":
                response['pekerjaan'] = f"""'{response['pekerjaan']}'"""
            add_tokoh(request.session['username'], response)
            return redirect('/tokoh/list')

    response['form'] = form
    response['role'] = request.session['role']

    return render(request, 'buat_tokoh.html', context = response)

def check_name_dups(username, name):
    with connection.cursor() as cursor:
        query = f"""select nama from cims.tokoh
        where username_pengguna = '{username}'
        and nama = '{name}';"""
        cursor.execute(query)
        out = cursor.fetchall()
        if (len(out) == 0):
            return ""
        else:
            return "Nama tokoh tidak boleh sama dari tokoh yang sudah ada dari pemilik yang sama"

def add_tokoh(username, response):
    with connection.cursor() as cursor:
        query = f"""insert into cims.tokoh values
        ('{username}',
         '{response['nama_tokoh']}',
         '{response['kelamin']}',
         'Aktif',
         0,
         100,
         0,
         0,
         '{response['kulit']}',
         1,
         '{response['sifat']}',
         {response['pekerjaan']},
         'rb001',
         'mt001',
         'rm001');"""
        cursor.execute(query)

def update_tokoh(request):
    if 'username' not in request.session:
        return redirect('/login')

    if (request.session['role'] == 'admin'):
        return redirect('/denied')

    u = request.session['username']
    t = request.GET.get("t", "")

    response = {}
    if (u != "" and t != ""):
        form = UpdateTokohForm(u, t, request.POST or None)
        if (request.method == 'POST' and form.is_valid()):
            set_tokoh(u, t,
                    request.POST['rambut'],
                    request.POST['mata'],
                    request.POST['rumah'])
            # Yeah, I know, it should be /tokoh/list
            # But it is much more sense to redirect the user to detail tokoh instead.
            # If you still insist about it, uncomment the line below and comment the other one.
            #return redirect('/tokoh/list')
            return redirect(f"""/tokoh/detail?u={u}&t={t}""")
    else:
        return redirect('/error')

    response['form'] = form
    response['tokoh'] = t
    response['role'] = request.session['role']

    return render(request, 'update_tokoh.html', context = response)

def set_tokoh(u, t, rb, mt, rm):
    with connection.cursor() as cursor:
        # Setting query requires to be set ONE BY ONE
        # I'm too lazy for checking if tokoh exists but since we get here by clicking on the update button,
        # We shouldn't worry about it too much.
        query = f"""update cims.tokoh
        set id_rambut = '{rb}'
        where username_pengguna = '{u}'
        and nama = '{t}';
        update cims.tokoh
        set id_mata = '{mt}'
        where username_pengguna = '{u}'
        and nama = '{t}';
        update cims.tokoh
        set id_rumah = '{rm}'
        where username_pengguna = '{u}'
        and nama = '{t}';"""
        cursor.execute(query)
