from django.urls import path
from . import views

app_name = 'bekerja'

urlpatterns = [
    path('', views.redirect_bekerja, name='redirectbekerja'),
    path('create', views.createbekerja, name='createbekerja'),
    path('execute', views.executecreate, name='executecreate'),
    path('list', views.readbekerja, name='listbekerja')
]
