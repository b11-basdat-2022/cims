from django.shortcuts import render, redirect
from django.db import connection
from collections import namedtuple

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def redirect_bekerja(request):
    return redirect('/bekerja/list')

def createbekerja(request):
    if 'username' not in request.session:
        return redirect('/login')

    if (request.session['role'] == 'admin'):
        return redirect('/denied')

    response = { 
            'name': request.session['username'],
            'role': request.session['role']
            }
    
    cursor = connection.cursor()
    try:
        cursor.execute("SET SEARCH_PATH TO CIMS")
        cursor.execute(
            f"""SELECT T.nama, T.pekerjaan, P.base_honor
                FROM TOKOH T, PEKERJAAN P
                WHERE T.username_pengguna='{request.session['username']}' AND T.pekerjaan=P.nama""")
        response['buatbekerja'] = namedtuplefetchall(cursor)
    except Exception as e:
        print(e)
    finally:
        cursor.close()

    return render(request, 'createbekerja.html', context = response)

def executecreate(request):
    if 'username' not in request.session:
        return redirect('/login')

    if (request.session['role'] == 'admin'):
        return redirect('/denied')

    tokoh = request.GET.get("tokoh", "")
    pekerjaan = request.GET.get("pekerjaan", "")
    keberangkatan = get_keberangkatan(request.session['username'], tokoh) + 1

    if (tokoh != "" and pekerjaan != ""):
        with connection.cursor() as cursor:
            query = f"""INSERT INTO CIMS.BEKERJA VALUES
            ('{request.session['username']}', '{tokoh}', DATE_TRUNC('second', CURRENT_TIMESTAMP::timestamp), '{pekerjaan}', {keberangkatan});"""
            cursor.execute("SET TIMEZONE=7;")
            cursor.execute(query)
        return redirect('/bekerja/list')
    else:
        return redirect('/error')

def readbekerja(request):
    if 'username' not in request.session:
        return redirect('/login')

    response = { 
            'name': request.session['username'],
            'role': request.session['role']
            }

    cursor = connection.cursor()
    try:
        cursor.execute("SET SEARCH_PATH TO CIMS")
        if (request.session['role'] == 'admin'):
            cursor.execute(
                f"""SELECT B.username_pengguna, B.nama_tokoh, B.timestamp, B.nama_pekerjaan, B.keberangkatan_ke, P.base_honor*T.level AS honor
                    FROM BEKERJA B, TOKOH T, PEKERJAAN P
                    WHERE B.nama_tokoh=T.nama AND B.nama_pekerjaan=P.nama""")
        else:
            cursor.execute(
                f"""SELECT B.username_pengguna, B.nama_tokoh, B.timestamp, B.nama_pekerjaan, B.keberangkatan_ke, P.base_honor*T.level AS honor
                    FROM BEKERJA B, TOKOH T, PEKERJAAN P
                    WHERE B.username_pengguna='{request.session['username']}' AND B.nama_tokoh=T.nama AND B.nama_pekerjaan=P.nama""")
        response['bekerja'] = namedtuplefetchall(cursor)
    except Exception as e:
        print(e)
    finally:
        cursor.close()

    return render(request, 'listbekerja.html', context = response)

def get_keberangkatan(username, nama):
    with connection.cursor() as cursor:
        query = f"""SELECT keberangkatan_ke FROM CIMS.BEKERJA
        WHERE username_pengguna='{username}' AND nama_tokoh='{nama}'
        ORDER BY keberangkatan_ke DESC
        LIMIT 1;"""
        cursor.execute(query)
        out = cursor.fetchall()

        if (len(out) == 0):
            return 0
        return out[0][0]
