import json
from django.shortcuts import render, redirect
from django.db import connection
from collections import namedtuple
from menggunakan_barang.forms import MenggunakanBarangForm

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def redirect_menggunakanbarang(request):
    return redirect('/menggunakan-barang/list')

def createmenggunakanbarang(request):
    if 'username' not in request.session:
        return redirect('/login')

    if (request.session['role'] == 'admin'):
        return redirect('/denied')

    response = { 
            'name': request.session['username'],
            'role': request.session['role']
            }

    tokoh_list, tokoh_choice, barang_dict, barang_all_choice = get_tokoh_barang(request.session['username'])

    form = MenggunakanBarangForm(tokoh_choice, barang_all_choice, request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        if 'barang' in request.POST:
            username = request.session['username']
            tokoh = request.POST['tokoh']
            id = request.POST['barang'] 
            response['error'] = can_use_barang(username, tokoh, id)
            if response['error'] == "":
                with connection.cursor() as cursor:
                    query = f"""INSERT INTO CIMS.MENGGUNAKAN_BARANG VALUES
                    ('{username}', '{tokoh}', DATE_TRUNC('second', CURRENT_TIMESTAMP::timestamp),'{id}');"""
                    cursor.execute("SET TIMEZONE=7;")
                    cursor.execute(query)
                return redirect('/menggunakan-barang/list')
        else:
            response['error'] = "Barang tidak boleh kosong"

    # JSON
    response['form'] = form
    response['json_tokoh_list'] = json.dumps(tokoh_list)
    response['json_tokoh_barang'] = json.dumps(barang_dict)

    return render (request, 'createmenggunakanbarang.html', context = response)

def readmenggunakanbarang(request):
    if 'username' not in request.session:
        return redirect('/login')

    response = { 
            'name': request.session['username'],
            'role': request.session['role']
            }

    cursor = connection.cursor()
    try:
        cursor.execute("SET SEARCH_PATH TO CIMS")
        if (request.session['role'] == 'admin'):
            cursor.execute(
                f"""SELECT *
                    FROM MENGGUNAKAN_BARANG, KOLEKSI_JUAL_BELI
                    WHERE id_barang=id_koleksi""")
        else:
            cursor.execute(
                f"""SELECT *
                    FROM MENGGUNAKAN_BARANG, KOLEKSI_JUAL_BELI
                    WHERE username_pengguna='{request.session['username']}' AND id_barang=id_koleksi""")
        response['menggunakanbarang'] = namedtuplefetchall(cursor)
    except Exception as e:
        print(e)
    finally:
        cursor.close()

    return render(request, 'listmenggunakanbarang.html', context = response)

def get_tokoh_barang(username):
    tokoh_list = []
    tokoh_choice = [(None,"---")]
    barang_dict = {}
    barang_all_choice = []

    with connection.cursor() as cursor:

        # Ambil semua nama tokoh yang dimiliki pemain yang sedang login
        query = f"""SELECT nama FROM CIMS.TOKOH
        WHERE username_pengguna = '{username}';"""
        cursor.execute(query)
        temp_list = double_list_converter(cursor.fetchall())
        tokoh_list.extend(temp_list[0])
        tokoh_choice.extend(temp_list[1])
        
        # Ambil semua id_barang yang dimiliki tokoh tersebut
        for tokoh in tokoh_choice:
            temp_tokoh = tokoh[0]
            query = f"""SELECT id_koleksi FROM CIMS.KOLEKSI_TOKOH
            WHERE id_koleksi LIKE 'br%'
            AND username_pengguna = '{username}'
            AND nama_tokoh = '{temp_tokoh}';"""
            cursor.execute(query)
            temp_list = double_list_converter(cursor.fetchall())
            barang_dict[temp_tokoh] = temp_list[0]
            barang_all_choice.extend(temp_list[1])

    return tokoh_list, tokoh_choice, barang_dict, barang_all_choice

def can_use_barang(username, tokoh, id):
    
    # Cek apakah energi tokoh mencukupi
    with connection.cursor() as cursor:
        query = f"""SELECT energi FROM CIMS.TOKOH
        WHERE username_pengguna = '{username}' AND nama = '{tokoh}';"""
        cursor.execute(query)
        energi_tokoh = cursor.fetchall()[0][0]

        query = f"""SELECT tingkat_energi FROM CIMS.BARANG
        WHERE id_koleksi = '{id}';"""
        cursor.execute(query)
        energi_barang = cursor.fetchall()[0][0]

        if (energi_tokoh < energi_barang):
            return f"""Energi tokoh '{tokoh}' tidak mencukupi sehingga barang dengan id '{id}' tidak dapat digunakan"""

    # Cek apakah tokoh sudah menggunakan barang tersebut
    with connection.cursor() as cursor:
        query = f"""SELECT * FROM CIMS.MENGGUNAKAN_BARANG
        WHERE username_pengguna = '{username}'
        AND nama_tokoh = '{tokoh}'
        AND id_barang = '{id}';"""
        cursor.execute(query)
        if len(cursor.fetchall()) == 0:
            return ""
        else:
            return f"""{tokoh} sudah menggunakan barang dengan id '{id}'"""

def double_list_converter(single_list):
    double_list = [[],[]]
    for row in single_list:
        double_list[0].append(row[0])
        double_list[1].append((row[0], row[0]))
    return double_list
