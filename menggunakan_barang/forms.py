from django import forms

class MenggunakanBarangForm(forms.Form):
    def __init__(self, tokoh, barang, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['tokoh'] = forms.CharField(
            label = 'Nama Tokoh',
            required = True,
            widget = forms.Select(choices = tokoh)
        )
        self.fields['barang'] = forms.CharField(
            label = 'Barang',
            required = False, # Di-handle di views
            widget = forms.Select(choices = barang)
        )