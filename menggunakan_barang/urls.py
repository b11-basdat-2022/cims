from django.urls import path
from . import views

app_name = 'menggunakan_barang'

urlpatterns = [
    path('', views.redirect_menggunakanbarang, name='redirectmenggunakanbarang'),
    path('create', views.createmenggunakanbarang, name='createmenggunakanbarang'),
    path('list', views.readmenggunakanbarang, name='listmenggunakanbarang')
]
