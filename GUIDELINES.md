# Guidelines
1. Ikuti aturan repo di bawah sebelum berinteraksi dengan repo ini.
2. Karena penggunaan bahasa dalam repo ini sangat berantakan, kalian bebas menggunakan bahasa Inggris maupun Indonesia. Asal jangan pakai bahasa Jaksel.
3. Lebih baik semua file ditulis dalam lowercase. Heroku dan sistem Linux saya membedakan folder `a` dan `A`.
4. Buat kodingan anda seminimal mungkin jika bisa. Catatan bahwa codebase yang kecil bukan berarti kodingan yang efisien. Ingat pelajaran SDA!
5. Dilarang menggunakan apapun yang tidak baik untuk privacy pengguna seperti iklan, tracker, dan analitik dari Google.
6. Gunakan library external atau API yang berlisensi Open Source (MIT atau GPL).
7. Jika ada file yang merasa tidak penting seperti `.idea`. Hapus file tersebut atau masukan ke `.gitignore`.

# Aturan Repo
Buatlah branch baru dari `staging`, **bukan** `master`, untuk setiap fitur yang kamu buat dengan nama yang sesuai (penamaan terserah kalian asal jelas). Jika anda merasa sudah selesai untuk sesi sekarang, kalian buat commit dengan pesan yang jelas (jangan "add" aja), push ke gitlab, dan buat merge request ke `staging` supaya saya bisa mudah melihat perubahan yang ada. Saya akan urus semua merge conflict jika ada. **Tolong jangan merge sendiri merge request kalian**. Chat ke saya terlebih dahulu.

Selalu berbasis branch anda dari commit `staging` yang terbaru. Saya capek mengupdate ulang kodingan yang hilang dari commit anda yang berbasis dari commit `staging` lama.

Jangan langsung push ke `staging` maupun `master`! Selalu melakukan merge request ke branch `staging`, bukan `master`. Ini untuk mencegah kekacauan besar pada webapp heroku jika ada sesuatu yang rusak yang masuk ke `master`. Jika `staging` sudah merasa aman, aku merge ke `master` dan perubahan dapat dilihat di herokuapp. 

# Aturan Kodingan
Untuk mengurangi beban saya, tolong ikuti semua aturan di bawah ini (optional berarti terserah anda). Saya akan enggan untuk menerima merge request anda jika beberapa aturan ini dilewatkan.

1. Saat mengedit `settings.py`, **pastikan** hanya list `INSTALLED_APPS` yang terubah.
2. Tulis semua link di `urls.py` dengan cara sebagai berikut:
    1. Dalam folder `fitur`, buat `urls.py` seperti contoh berikut:
    ```python
    from django.urls import path
    from .views import fitur_daftar, fitur_buat, ...

    urlpatterns = [
        path('daftar', fitur_daftar, name='fitur-daftar'),
        path('buat', fitur_buat, name='fitur-buat'),
        ...
    ]
    ```
    2. Pada `urls.py` di folder `cims_b11`, Tambahkan dua line berikut untuk import dan list urls:
    ```python
    # import
    import fitur.urls as fitur

    # urlpatterns
    path('fitur/', include(fitur))
    ```
    3. Masing-masing link akan dapat diakses melalui `fitur/daftar`, `fitur/buat`, dan `fitur/seterusnya`.
    4. Untuk pengambilan query/argument dari url, saya bebaskan cara kalian untuk mendapatkannya. Rekomendasi saya adalah menggunakan `request.GET.get()` karena mudah ditulis dan kalian tidak perlu mengubah lagi `urls.py` kalian. Contoh query url adalah `fitur/detail?q=satu&r=dua` dan di views kalian bisa tulis `request.GET.get("q","")` dan `request.GET.get("r","")` untuk mengaksesnya. Contohnya ada di [views tokoh](tokoh/views.py).
    5. [OPTIONAL] Anda boleh buat suatu link redirect di mana saat pengguna buka link `fitur/` tanpa tambahan apapun setelah tanda "/", pengguna akan diarahkan ke page yang anda sarankan. Misalnya, pengguna buka `fitur/`. Anda boleh redirect mereka ke `fitur/daftar` dari link tersebut. Contohnya ada di [views signup](signup/views.py) dan [urls signup](signup/urls.py).
    6. [OPTIONAL] Anda juga boleh redirect pengguna ke `error/` atau `denied/` untuk aksi yang menghasilkan error atau role yang tidak diperkenankan. Gunakan sebaiknya.
3. **JANGAN LUPA menulis link anda pada [`templates/navbar.html`](templates/navbar.html) sesuai tempatnya (pemain dan admin)**
4. Untuk html, anda **harus** meng-extend `main.html`, include `navbar.html`, dan menggunakan resource css/js (termasuk bootstrap) yang sudah ada di dalam folder templates dan static di root projek. Dengan kata lain, kalian harus menggunakan template berikut untuk setiap file html yang dibuat:

    ```html
    {% extends 'main.html' %} {% load static %} 

    {% block meta %}
    <title>Nama Page Kamu - The Cims</title>
    {% endblock meta %}

    {% block content %}
    {% firstof "NAMA FITUR" as nav_title %}
    {% include 'navbar.html' %}
    <!--
    Tempat kalian menulis <body> tanpa menggunakan tag <body>
    -->
    {% endblock content %}

    ```
5. Jika ingin menambahkan css, js, atau asset lainya, masuki mereka ke folder `static` dan subfolder yang sesuai. Untuk css, anda boleh mengedit langsung `main.css` atau buat sendiri. Jika nama css tambahan anda `fitur.css`, tambah baris di bawah ini tepat setelah tag `<title>` dan sebelum `{% endblock meta %}`
    ```html
    <link rel="stylesheet" href="{% static 'css/fitur.css' %}">
    ```
6. Jika ingin menambahkan `fitur.js`, tambah baris di bawah ini di akhir file html anda:
    ```html
    {% block script %}
    <script src="{% static 'scripts/fitur.js' %}"></script>
    {% endblock script %}
    ```
7. Ada beberapa aturan untuk menulis `views.py`.
    1. Selalu awali function render html anda dengan snippet di bawah ini untuk mencegah akses pengguna yang belum login ke The Cims. Page yang boleh diakses tanpa akun hanyalah home, login, dan signup.
    ```python
    if 'username' not in request.session:
        return redirect('/login')
    ```
    2. Kalian harus memasukan key `role` dengan value `request.session['role']` pada argument context saat meng-return render. Ini supaya navbar dapat mengecek status pengguna (navbar untuk admin dan pemain beda). Contohnya ada di [views login](login/views.py) atau [views misi](misi/views.py).
    3. Saat menggunakan cursor, selalu tulis dengan `with connection.cursor() as cursor:` untuk mengurangi kemungkinan kemunculan error misterius "cursor already closed". Contohnya adalah sebagai berikut: 

    ```python
    # Versi biasa
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM CIMS.TABLE;")
        result = cursor.fetchall() # return as tuple

    # Versi untuk anda yang paranoid error
    with connection.cursor() as cursor:
        try:
            cursor.execute("SELECT * FROM CIMS.TABLE;")
            result = cursor.fetchall()
        except Exception as e:
            print(e)
        finally:
            cursor.close()
    ```
    4. Ekstensi dari nomor iii tadi. Disarankan untuk tidak menulis `cursor.execute("SET search_path TO CIMS;")` setiap saat anda memanggil SQL query. Langsung saja memanggil tabel dengan `CIMS.TABLE` sepeti `CIMS.TOKOH`. Kalian juga bisa menuliskan dua query SQL berbeda dalam satu line (e.g. `SELECT * FROM CIMS.AKUN; SELECT * FROM CIMS.PEMAIN;`)

# Tips
Beberapa bantuan untuk mengerjakan fitur anda.

1. Gunakan `<th class="counterCell"></th>` (atau dengan tag `<td>`) untuk menambahkan increment nomor table secara otomatis. Contohnya ada di [template pekerjaan](pekerjaan/templates/listpekerjaan.html).
2. Untuk form yang cantik seperti pada login, ikuti contoh form seperti berikut:
```htm
<div align="center" class="container good-form">
    <h1>Judul Form</h1>
    <form action="" method="POST">
        {% csrf_token %}
        {% for field in form %}
        <label class="form-label">{{ field.label }}:</label>
        <br>
        {{ field }}
        <br><br>
        {% endfor %}
        <button class="btn btn-primary" name='login' type='submit'>Submit</button>
        {% if error_msg %}
        <br><br>
        <p style="color: red;">{{ error_msg }}</p>
        {% endif %}
    </form>
</div>
```
3. Jika ada suatu form yang membutuhkan dropdown yang secara dinamis berubah dari dropdown sebelumnya, kalian bisa ikuti [views menggunakan apparel](use_apparel/views.py) untuk django/sql dan [template menggunakan apparel](use_apparel/templates/buat_mengapp.html) untuk javascript. Sumber implementasinya dapat dilihat [di sini](https://github.com/Tauhid-UAP/Dependent-Dropdown-Tutorial).

# Resource
Semua resource untuk mengerjakan The Cims:
- [Deskripsi umum untuk The Cims (PDF Scele)](https://scele.cs.ui.ac.id/pluginfile.php/141079/mod_resource/content/8/Soal%20Tugas%20Kelompok%201%20-%20Revisi%201.pdf)
- [Relational Diagram untuk The Cims (PDF Scele)](https://scele.cs.ui.ac.id/pluginfile.php/144783/mod_resource/content/2/Relational%20Diagram%20Case%201%20-%20The%20Cims.pdf)
- [Deskripsi Aplikasi untuk The Cims (Google Docs)](https://docs.google.com/document/d/1dH5FP5ME89LcE4B34XaGdNowx8HhjdKJBl76B5fCoyE)
- [Data Dummy (Google Sheets)](https://docs.google.com/spreadsheets/d/1pYq2A3_VTh6mBCoXxdC40CTGaIhVmbHO/edit#gid=1209728601)
- [Create Table (Google Docs)](https://docs.google.com/document/d/13QpZ0YvJllPfCyyqrXZUT4lqPyYmGBXIQ0i6Mqmr328/edit)
- [Insert Into Table (Goole Docs)](https://docs.google.com/document/d/1sGztPTitsAE4uNpXWG2-qkz1neb77wTrgRSAXdjnx2o/edit)
