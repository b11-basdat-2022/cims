from django.shortcuts import render
from django.http import HttpResponse

def dummy(request):
    return render_error_page(request, 'dummy')

def access_denied(request):
    return render_error_page(request, 'deny')

def error_generic(request):
    return render_error_page(request, 'error')

def render_error_page(request, error_type):
    response = { 'error': error_type }
    if 'role' in request.session:
        response['role'] = request.session['role']
    return render(request, 'dummy.html', context = response)

