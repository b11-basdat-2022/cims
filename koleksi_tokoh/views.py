from django.shortcuts import redirect, render
from django.http.response import HttpResponse
from django.core import serializers
from django.db import IntegrityError, connection
from collections import namedtuple
from django.contrib import messages

# Create your views here.

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def read_koleksi_tokoh(request):
    if 'username' not in request.session:
        return redirect('/login')

    if request.session['role'] == 'pemain':
        with connection.cursor() as cursor:
            try:
                query = f"""SELECT nama_tokoh, id_koleksi
                FROM CIMS.KOLEKSI_TOKOH WHERE
                username_pengguna = '{request.session['username']}';"""
                cursor.execute(query)
                result = namedtuplefetchall(cursor)
            except Exception as e:
                print(e)
            finally:
                cursor.close()

            return render(request, 'list_koleksi_tokoh.html', {'result': result, 'role': request.session['role']})
            
    if request.session['role'] == 'admin':
        with connection.cursor() as cursor:
            try:
                query = f"""SELECT username_pengguna, nama_tokoh, id_koleksi
                FROM CIMS.KOLEKSI_TOKOH;"""
                cursor.execute(query)
                result = namedtuplefetchall(cursor)
            except Exception as e:
                print(e)
            finally:
                cursor.close()
                
            return render(request, 'list_koleksi_tokoh.html', {'result': result, 'role': request.session['role']})

def create_koleksi_tokoh(request):
    if 'username' not in request.session:
        return redirect('/login')

    if (request.session['role'] == 'admin'):
        return redirect('/denied')

    with connection.cursor() as cursor:
        result = None
        tokoh = None
        id = None
        try:
            # if request.method == 'POST':
            #     nama_tokoh = request.POST['tokoh']
            #     id_koleksi = request.POST['id']
            #     if nama_tokoh == " " or id_koleksi == " ":
            #         messages.info(request, 'Data insufficient')
            #     else :
            #         return redirect('/koleksiTokoh/create-koleksi-tokoh')
        

            if request.session['role'] == 'admin':
                return redirect('/koleksiTokoh/create-koleksi-tokoh')
            
            query = f"""SELECT nama
                   FROM CIMS.TOKOH WHERE
                   username_pengguna = '{request.session['username']}';"""
            cursor.execute(query)
            tokoh = namedtuplefetchall(cursor)

            query = f"""SELECT id FROM CIMS.KOLEKSI;"""
            cursor.execute(query)
            id = namedtuplefetchall(cursor)

            username_pengguna = request.session['username']
            nama_tokoh = request.GET['tokoh']
            id_koleksi = request.GET['id']
            cursor.execute("INSERT INTO CIMS.KOLEKSI_TOKOH VALUES (%s, %s, %s)",[id_koleksi, username_pengguna, nama_tokoh])

        except Exception as e:
            print(e)
        finally:
            cursor.close()

        if result != None:
            return render(request, 'create_koleksi_tokoh.html', {'result' : result, 'tokoh' : tokoh, 'id' : id , 'role': request.session['role']})
        else :
            return render(request, 'create_koleksi_tokoh.html', {'tokoh' : tokoh, 'id' : id , 'role': request.session['role']})  

