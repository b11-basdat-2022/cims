from django.urls import path
from .views import read_koleksi_tokoh, create_koleksi_tokoh

urlpatterns = [
    path('daftar-koleksi-tokoh', read_koleksi_tokoh, name='daftar-koleksi-tokoh'),
    path('create-koleksi-tokoh', create_koleksi_tokoh, name='create-koleksi-tokoh'),
]