# from django import forms
# from django.db import connection
# from collections import namedtuple

# def namedtuplefetchall(cursor):
#     "Return all rows from a cursor as a namedtuple"
#     desc = cursor.description
#     nt_result = namedtuple('Result', [col[0] for col in desc])
#     return [nt_result(*row) for row in cursor.fetchall()]

# def get_nama_tokoh(request):
#     with connection.cursor() as cursor:
#         query = f"""SELECT nama
#                 FROM CIMS.TOKOH WHERE
#                 username_pengguna = '{request.session['username']}';"""
#         cursor.execute(query)
#         result = namedtuplefetchall(cursor)
#     temp_res = [result[i]['nama'] for i in range(len(result))]
#     return [tuple([i,i]) for i in temp_res]

# def get_id_koleksi():
#     with connection.cursor() as cursor:
#         query = f"""SELECT id FROM CIMS.KOLEKSI';"""
#         cursor.execute(query)
#         result = namedtuplefetchall(cursor)
#     temp_res = [result[i]['id'] for i in range(len(result))]
#     return [tuple([i,i]) for i in temp_res]

# class CreateKoleksiTokoh(forms.Form):
#     nama_tokoh = forms.CharField(
#         label= 'Nama Tokoh',
#         required = True,
#     )

#     id_koleksi = forms.CharField(
#         label= 'Id Koleksi',
#         required = True,
#     )